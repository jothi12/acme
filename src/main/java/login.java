import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class login {
	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver drivers= new ChromeDriver();
	    drivers.manage().window().maximize();
	    drivers.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		drivers.get("https://acme-test.uipath.com/account/login");
		//drivers.findElementByXPath((//input[@class='form-control'])[1])
	    drivers.findElementById("email").sendKeys("kumar.testleaf@gmail.com");
	    drivers.findElementById("password").sendKeys("leaf@12");
	    drivers.findElementById("buttonLogin").click();
	    drivers.findElementByXPath("(//div[@class='dropdown']//button)[5]").click();
	    drivers.findElementByXPath("//a[@href='/vendors/search']").click();
	    drivers.findElementByXPath("(//input[@class='form-control'])[2]").click();
	    drivers.findElementById("vendorName").sendKeys("Blue Lagoon");
	    drivers.findElementById("buttonSearch").click();
	    //drivers.findElementsByXPath("(//table[@class='table']//th)[5]");
	    WebElement findCountry =  drivers.findElementByXPath("/html/body/div/div[2]/div/table/tbody/tr[2]/td[5]");
	    System.out.println(findCountry.getText());
	    drivers.findElementByLinkText("Log Out").click();
	    drivers.close();
	}
	

}
